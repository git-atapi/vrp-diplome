/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reports;

/**
 *
 * @author khaled
 */
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfAConformanceLevel;
import com.itextpdf.text.pdf.PdfAWriter;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import java.awt.Font;
import static java.awt.PageAttributes.MediaType.A3;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;

public class PrintCheck {

    private String filePath;

    public PrintCheck(String name) {
        this.filePath = name + ".pdf";
        SwingController controller = new SwingController();

        SwingViewBuilder factory = new SwingViewBuilder(controller);

        JPanel viewerComponentPanel = factory.buildViewerPanel();

        controller.getDocumentViewController().setAnnotationCallback(
                new org.icepdf.ri.common.MyAnnotationCallback(
                        controller.getDocumentViewController()));

        JFrame applicationFrame = new JFrame();
        applicationFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //applicationFrame.getContentPane().add(viewerComponentPanel);
        applicationFrame.add(viewerComponentPanel);

        controller.openDocument(filePath);

        applicationFrame.pack();
        applicationFrame.setVisible(true);
    }

    public static void printer(int x, ArrayList<TypePrinter> obj) throws DocumentException {

        Document doc = new Document(PageSize.A5,10, 10, 10, 10);
        try {
            PdfWriter.getInstance(doc, new FileOutputStream("report.pdf"));
            doc.open();

            doc.add(new Paragraph(" "
                    + "word", FontFactory.getFont(FontFactory.TIMES_ITALIC, 18, Font.BOLD, BaseColor.RED)));
            Paragraph p = new Paragraph();

            Paragraph preface = new Paragraph("new biin");
            preface.setAlignment(Element.ALIGN_RIGHT);
            doc.add(preface);

            Chunk glue = new Chunk(new VerticalPositionMark());
            Paragraph p3 = new Paragraph("Text to the left");
            p3.add(new Chunk(glue));
            p3.add("Text to the right\n\n\n\n");
            doc.add(p3);

            PdfPTable table = new PdfPTable(x);
            table.setWidthPercentage(100);
            table.getDefaultCell().setUseAscender(true);
            table.getDefaultCell().setUseDescender(true);
            //Font f = new Font(FontFamily.HELVETICA, 13, Font.NORMAL, GrayColor.GRAYWHITE);
            PdfPCell cell1 = new PdfPCell(new Phrase("This is a header"));
            cell1.setColspan(4);

            cell1.setBackgroundColor(GrayColor.WHITE);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

            table.addCell(cell1);
            table.getDefaultCell().setBackgroundColor(new GrayColor(0.75f));

            float[] columnWidths = new float[]{25f, 10f, 10f, 10f};
            table.setWidths(columnWidths);

            for (int i = 0; i < obj.size(); i++) {
                PdfPCell cell = new PdfPCell(new Paragraph(obj.get(i).getValue()));
                if (obj.get(i).getSign().equals("montant")) {
                    DecimalFormat decimalPrintFormat = new DecimalFormat("#,##0.00####");
                    String retour = "";
                    retour = decimalPrintFormat.format(Double.valueOf(obj.get(i).getValue()));

                    cell = new PdfPCell(new Paragraph(retour));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

                }
                if (isNumeric(obj.get(i).getValue())) {
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                }

                table.addCell(cell);

                table.setWidths(columnWidths);

            }
            doc.add(table);
            doc.close();

        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ""
                    + Logger.getLogger(PrintCheck.class.getName()).toString());

        }

        // step 1
        // step 5
        new PrintCheck("report");

        
    }

    /*
        try {
            try {

                PdfPTable table = new PdfPTable(x);
                table.setWidthPercentage(100);
                table.getDefaultCell().setUseAscender(true);
                table.getDefaultCell().setUseDescender(true);
                //Font f = new Font(FontFamily.HELVETICA, 13, Font.NORMAL, GrayColor.GRAYWHITE);
                PdfPCell cell1 = new PdfPCell(new Phrase("This is a header"));
                cell1.setColspan(4);

                cell1.setBackgroundColor(GrayColor.WHITE);
                cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

                table.addCell(cell1);
                table.getDefaultCell().setBackgroundColor(new GrayColor(0.75f));

                float[] columnWidths = new float[]{25f, 10f, 10f, 10f};
                table.setWidths(columnWidths);
                for (int i = 0; i < obj.size(); i++) {
                    PdfPCell cell = new PdfPCell(new Paragraph(obj.get(i).getValue()));
                    if (obj.get(i).getSign().equals("montant")) {
                        DecimalFormat decimalPrintFormat = new DecimalFormat("#,##0.00####");
                        String retour = "";
                        retour = decimalPrintFormat.format(Double.valueOf(obj.get(i).getValue()));

                        cell = new PdfPCell(new Paragraph(retour));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

                    }
                    if (isNumeric(obj.get(i).getValue())) {
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    }

                    table.addCell(cell);

                    table.setWidths(columnWidths);
                    doc.add(table);

                }

            } catch (DocumentException ex) {
                Logger.getLogger(PrintCheck.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PrintCheck.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        /*

        new PrintCheck("report");
        //new PrintCheck("a3");
    }
     */
    public static boolean isNumeric(String str) {
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }
}

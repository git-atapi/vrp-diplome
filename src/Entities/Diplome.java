/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author khaled
 */
public class Diplome {
    private int id;
    private String nom;
    private String prenom;
    private String date_nec;
    private String lieu_nec;
    private String type_dip;
    private String faculte;

    public Diplome() {
    }

    public Diplome(int id, String nom, String prenom, String date_nec, String lieu_nec, String type_dip, String faculte) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.date_nec = date_nec;
        this.lieu_nec = lieu_nec;
        this.type_dip = type_dip;
        this.faculte = faculte;
    }

    public Diplome(String nom, String prenom, String date_nec, String lieu_nec, String type_dip, String faculte) {
        this.nom = nom;
        this.prenom = prenom;
        this.date_nec = date_nec;
        this.lieu_nec = lieu_nec;
        this.type_dip = type_dip;
        this.faculte = faculte;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDate_nec() {
        return date_nec;
    }

    public void setDate_nec(String date_nec) {
        this.date_nec = date_nec;
    }

    public String getLieu_nec() {
        return lieu_nec;
    }

    public void setLieu_nec(String lieu_nec) {
        this.lieu_nec = lieu_nec;
    }

    public String getType_dip() {
        return type_dip;
    }

    public void setType_dip(String type_dip) {
        this.type_dip = type_dip;
    }

    public String getFaculte() {
        return faculte;
    }

    public void setFaculte(String faculte) {
        this.faculte = faculte;
    }
    
    

}

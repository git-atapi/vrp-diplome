/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Outils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author khaled
 */
public class Outils {

    public static String setDate(String d) {
        d = d.replaceAll("-", "/");
        d = new StringBuilder(d).reverse().toString();

        String temp = "", temp2 = "", temp3 = "";
        for (int i = 6; i < d.length(); i++) {
            temp = temp + d.charAt(i);
        }
        temp = new StringBuilder(temp).reverse().toString();

        for (int i = 0; i < 2; i++) {
            temp2 = temp2 + d.charAt(i);
        }

        temp3 = "" + d.charAt(3) + d.charAt(4);

        temp2 = new StringBuilder(temp2).reverse().toString();
        temp3 = new StringBuilder(temp3).reverse().toString();

        return temp2 + temp3 + temp;
    }

    public static String testDate(String date) {
        date = date.replaceAll("/", "-");
        boolean just = false;
        String a = "", m = "", j = "";
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            df.setLenient(false);
            df.parse(date);
            just = true;
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(null, "Date non valid", "Error", JOptionPane.ERROR_MESSAGE);
            just = false;
            return null;
        }

        if (just == true) {
            a = "" + date.charAt(6) + date.charAt(7) + date.charAt(8) + date.charAt(9);
            m = "" + date.charAt(3) + date.charAt(4);
            j = "" + date.charAt(0) + date.charAt(1);

        }
        return a + "-" + m + "-" + j;
    }

    public static String SQLDate(String date) {
        date = date.replaceAll("/", "-");
        boolean just = false;
        String a = "", m = "", j = "";
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            df.setLenient(false);
            df.parse(date);
            just = true;
        } catch (ParseException e) {
            //JOptionPane.showMessageDialog(null, "Date non valid", "Error", JOptionPane.ERROR_MESSAGE);
            just = false;
            return null;
        }

        if (just == true) {
            a = "" + date.charAt(6) + date.charAt(7) + date.charAt(8) + date.charAt(9);
            m = "" + date.charAt(3) + date.charAt(4);
            j = "" + date.charAt(0) + date.charAt(1);

        }
        return a + "-" + m + "-" + j;
    }

    public static String testDate2(String date) {
        date = date.replaceAll("/", "-");
        boolean just = false;
        String a = "", m = "", j = "";
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            df.setLenient(false);
            df.parse(date);
            just = true;
        } catch (ParseException e) {
            just = false;
            return null;
        }

        if (just == true) {
            a = "" + date.charAt(6) + date.charAt(7) + date.charAt(8) + date.charAt(9);
            m = "" + date.charAt(3) + date.charAt(4);
            j = "" + date.charAt(0) + date.charAt(1);

        }
        return a + "-" + m + "-" + j;
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Outils;

import java.awt.Color;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author khaled
 */
public class OutilsGUI {
    public static String change = "";
    public static String paymentchange = "";
    public static String detailsSelectedFounisseur = "";
    public static boolean swapdebtfacture = false;
    public static boolean swapdebtbl = false;

    public static void setDefault(JPanel p1) {
        p1.setBackground(Color.decode("#f2f1f0"));
    }

    public static void setBtnsIN(JPanel p1) {
        p1.setBackground(Color.decode("#66B9D7"));
    }

    public static void setBtnsOUT(JPanel p1) {
        p1.setBackground(Color.decode("#1c4587"));
    }

    public static void setColor(JPanel panel) {
        panel.setBackground(new java.awt.Color(197, 197, 197));
    }

    public static void resetColor(JPanel panel) {
        panel.setBackground(new java.awt.Color(240, 240, 240));
    }

    public static void swap(JPanel p1, JPanel p2) {
        p1.removeAll();
        p1.repaint();
        p1.revalidate();
        p1.add(p2);
        p1.repaint();
        p1.revalidate();
    }
    
    public static JTable setTableRight(JTable table){
        TableColumnModel columnModel = table.getColumnModel();
        TableColumn column = columnModel.getColumn(2);
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(JLabel.RIGHT);
        column.setCellRenderer(renderer);
        return table;
    }
    
    
        public static String now() {
        Connection cnx = DataBaseCNX.getConnection();
        String sql = "SELECT date(NOW()) ";
        Statement st;
        try {
            st = cnx.createStatement();
            ResultSet rs = st.executeQuery(sql);

            // iterate through the java resultset
            while (rs.next()) {
                String res = rs.getString("date(NOW())");
                return res;
            }
        } catch (SQLException ex) {
            //Logger.getLogger(outils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public static String time(Connection cnx) {
        String sql = "SELECT time(NOW()) ";
        Statement st;
        try {
            st = cnx.createStatement();
            ResultSet rs = st.executeQuery(sql);

            // iterate through the java resultset
            while (rs.next()) {
                String res = rs.getString("time(NOW())");
                return res;
            }
        } catch (SQLException ex) {
            //Logger.getLogger(outils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Outils;

import javax.swing.JOptionPane;

/**
 *
 * @author khaled
 */
public class Exceptions {

    public static boolean except = true;

    public static void error(String s, String in) {
        if (s.equals("")) {
            JOptionPane.showMessageDialog(null, in + " non valid", "Error", JOptionPane.ERROR_MESSAGE);
            except = false;
        }
    }

    public static void warning(String s, String in) {
        if (s.equals("")) {
            int dialogResult = JOptionPane.showConfirmDialog(null, "facture sans " + in +" voulez-vous continuer", "Warning",
                    JOptionPane.WARNING_MESSAGE);
            if (dialogResult == JOptionPane.YES_OPTION) {
                // Saving code here
                except = true;            
            }else{
                except = false;            
            }
        }

    }

}
